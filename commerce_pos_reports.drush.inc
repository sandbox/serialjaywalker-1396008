<?php

/**
 * Implements hook_drush_command().
 */
function commerce_pos_reports_drush_command() {
  $items  = array();
  $items['commerce-pos-reports-csv'] = array(
    'callback' => 'commerce_pos_reports_drush_csv',
    'description' => dt('Generate CSV'),
    'arguments' => array(
      "start" => "start",
      "end" => "end",
      "interval" => "interval",
   ),
 );
 return $items;
}

/**
 * Implements hook_drush_help().
 */
function commerce_pos_reports_drush_help($section) {
  switch ($section) {
    case 'drush:commerce-pos-reports-csv':
      return dt("Generate a CSV report.");
  }
}

/**
 * Callback for drush command.
 */
function commerce_pos_reports_drush_csv($start, $end, $interval) {
  if (!intval($start)) {
    $start = strtotime($start);
  }
  if (!intval($end)) {
    $end = strtotime($end);
  }
  
  $query_filters = array();
  $filter_hash = md5(serialize($query_filters));
  cache_set('commerce_pos_reports_filters:' . $filter_hash, $query_filters);

  $batch = commerce_pos_reports_details_batch($start, $end, $interval, $filter_hash);
  //I couldn't get Drush batch processing to properly preserve context
  //from request to request, so we'll call the operations directly 
  //for now. Could cause problems with memory limit on large reports.
  $context = array();
  foreach($batch['operations'] as $operation) {
    $callback = $operation[0];
    $args = $operation[1];
    $args[] = &$context;
    call_user_func_array($callback, $args);
  }
  $finished = $batch['finished'];
  $success = TRUE;
  $context['results']['drush'] = TRUE;
  $finished($success, $context['results'], $batch['operations']);
  return commerce_pos_reports_details_csv_generate("details:$start:$end:$interval:$filter_hash");
}


