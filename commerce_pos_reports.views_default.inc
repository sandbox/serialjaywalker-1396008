<?php

function commerce_pos_reports_views_default_views() {
  $view = new view;
  $view->name = 'product_sales';
  $view->description = 'Gives the number of each product sold.';
  $view->tag = 'commerce';
  $view->base_table = 'commerce_line_item';
  $view->human_name = 'Product Sales';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Product sales';
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
  'quantity' => 'quantity',
  'product_id' => 'product_id',
  'title' => 'title',
  'commerce_total' => 'commerce_total',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
  'quantity' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'product_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  'commerce_total' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
  ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Relationship: Commerce Line Item: Order ID */
  $handler->display->display_options['relationships']['order_id']['id'] = 'order_id';
  $handler->display->display_options['relationships']['order_id']['table'] = 'commerce_line_item';
  $handler->display->display_options['relationships']['order_id']['field'] = 'order_id';
  $handler->display->display_options['relationships']['order_id']['required'] = 1;
  /* Relationship: Commerce Line item: Referenced product */
  $handler->display->display_options['relationships']['commerce_product_product_id']['id'] = 'commerce_product_product_id';
  $handler->display->display_options['relationships']['commerce_product_product_id']['table'] = 'field_data_commerce_product';
  $handler->display->display_options['relationships']['commerce_product_product_id']['field'] = 'commerce_product_product_id';
  $handler->display->display_options['relationships']['commerce_product_product_id']['required'] = 1;
  /* Field: SUM(Commerce Line Item: Quantity) */
  $handler->display->display_options['fields']['quantity']['id'] = 'quantity';
  $handler->display->display_options['fields']['quantity']['table'] = 'commerce_line_item';
  $handler->display->display_options['fields']['quantity']['field'] = 'quantity';
  $handler->display->display_options['fields']['quantity']['group_type'] = 'sum';
  $handler->display->display_options['fields']['quantity']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['external'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['quantity']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['quantity']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['quantity']['alter']['html'] = 0;
  $handler->display->display_options['fields']['quantity']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['quantity']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['quantity']['hide_empty'] = 0;
  $handler->display->display_options['fields']['quantity']['empty_zero'] = 0;
  $handler->display->display_options['fields']['quantity']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['quantity']['set_precision'] = 0;
  $handler->display->display_options['fields']['quantity']['precision'] = '0';
  $handler->display->display_options['fields']['quantity']['format_plural'] = 0;
  /* Field: Commerce Product: Product ID */
  $handler->display->display_options['fields']['product_id']['id'] = 'product_id';
  $handler->display->display_options['fields']['product_id']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['product_id']['field'] = 'product_id';
  $handler->display->display_options['fields']['product_id']['relationship'] = 'commerce_product_product_id';
  $handler->display->display_options['fields']['product_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['product_id']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['external'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['product_id']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['product_id']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['product_id']['alter']['html'] = 0;
  $handler->display->display_options['fields']['product_id']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['product_id']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['product_id']['hide_empty'] = 0;
  $handler->display->display_options['fields']['product_id']['empty_zero'] = 0;
  $handler->display->display_options['fields']['product_id']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['product_id']['link_to_product'] = 0;
  /* Field: Commerce Product: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'commerce_product';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'commerce_product_product_id';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_product'] = 1;
  /* Field: SUM(Commerce Line item: Total) */
  $handler->display->display_options['fields']['commerce_total']['id'] = 'commerce_total';
  $handler->display->display_options['fields']['commerce_total']['table'] = 'field_data_commerce_total';
  $handler->display->display_options['fields']['commerce_total']['field'] = 'commerce_total';
  $handler->display->display_options['fields']['commerce_total']['group_type'] = 'sum';
  $handler->display->display_options['fields']['commerce_total']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_total']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_total']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_total']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_total']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['commerce_total']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_total']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_total']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_total']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['commerce_total']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_total']['type'] = 'commerce_price_formatted_amount';
  $handler->display->display_options['fields']['commerce_total']['settings'] = array(
  'calculation' => FALSE,
  );
  $handler->display->display_options['fields']['commerce_total']['field_api_classes'] = 0;
  /* Contextual filter: Commerce Line item: Product (commerce_product) */
  $handler->display->display_options['arguments']['commerce_product_product_id']['id'] = 'commerce_product_product_id';
  $handler->display->display_options['arguments']['commerce_product_product_id']['table'] = 'field_data_commerce_product';
  $handler->display->display_options['arguments']['commerce_product_product_id']['field'] = 'commerce_product_product_id';
  $handler->display->display_options['arguments']['commerce_product_product_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['commerce_product_product_id']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['commerce_product_product_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['commerce_product_product_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['commerce_product_product_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['commerce_product_product_id']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['commerce_product_product_id']['not'] = 0;
  /* Filter criterion: Commerce Line Item: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'commerce_line_item';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
  'product' => 'product',
  );
  /* Filter criterion: Commerce Order: Order state */
  $handler->display->display_options['filters']['state']['id'] = 'state';
  $handler->display->display_options['filters']['state']['table'] = 'commerce_order';
  $handler->display->display_options['filters']['state']['field'] = 'state';
  $handler->display->display_options['filters']['state']['relationship'] = 'order_id';
  $handler->display->display_options['filters']['state']['value'] = array(
  'pending' => 'pending',
  'completed' => 'completed',
  );
  /* Filter criterion: Commerce Product: POS Categories (commerce_pos_category) */
  $handler->display->display_options['filters']['commerce_pos_category_tid']['id'] = 'commerce_pos_category_tid';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['table'] = 'field_data_commerce_pos_category';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['field'] = 'commerce_pos_category_tid';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['relationship'] = 'commerce_product_product_id';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['value'] = '';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['commerce_pos_category_tid']['expose']['operator_id'] = 'commerce_pos_category_tid_op';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['expose']['use_operator'] = 1;
  $handler->display->display_options['filters']['commerce_pos_category_tid']['expose']['operator'] = 'commerce_pos_category_tid_op';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['expose']['identifier'] = 'commerce_pos_category_tid';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['expose']['multiple'] = 1;
  $handler->display->display_options['filters']['commerce_pos_category_tid']['reduce_duplicates'] = 0;
  $handler->display->display_options['filters']['commerce_pos_category_tid']['vocabulary'] = 'commerce_pos_categories';
  $handler->display->display_options['filters']['commerce_pos_category_tid']['error_message'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/reports/product_sales';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Product sales';
  $handler->display->display_options['menu']['description'] = 'View the number of each product sold, and the revenue generated.';
  $handler->display->display_options['menu']['weight'] = '2';
  $handler->display->display_options['menu']['name'] = 'management';

  $views[$view->name] = $view;

  return $views;
}